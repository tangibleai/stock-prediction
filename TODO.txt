make new pipeline with one feature, AAPL close lagged once trying to predict AAPL close
only try to predict the last price in the entire dataset
after that try to predict the last price (deleting the last row in the dataset and running ml pipeline 
again)
continue to do this in a for loop until half the dataset is gone
make sure to record the predictions in a new list/series
then compare that test set to the truth with RMSE


week 8
create a function that takes a dataframe a makes a plot (the dataframe needs preds, truth, etc)
create a main function and call if from if __name__ = main (it takes no args). It should return a dict of the important variables (dfs, metrics, etc)
create 3 integer features for the month, day of the month, year, day of the year, day of the week, any other datetime features that I can find
leave date as the index
when plotting, don't make the x access the date index. x axis should be range(len(df))
plot function should take bool arg that is called use_index
keep track of each model in a table (use a spreadsheet) (track model class name (type of model), number of features, feature name(s) that were added to training data, rmse on training + test (seperate columns), include the one coef for the new feature)
if all done try lasso or svm
when using add column to spreadsheet called regularization strength


week 9
transpose hyperparameter table rows are experiments
delete any columns (hyperparameters) that are not valid or usefull
delete any experiments with invalid data (missing data, weird format, etc)
be able to answer questions about runs: why did the accuracy change from row 1 to row 2 (should be able to answer this on any pair of rows),
which features were most helpfull
was there a day of the week were tommorow's price was highest
how would you reccomend a trader use your model
reccomendations for future work: web traffic web data
go over and do the same thing for web traffic

web traffic data:
add a few other pages